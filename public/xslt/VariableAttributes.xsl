<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="applyVisibility">
        <xsl:param name="fullName" />
        <xsl:choose>
            <xsl:when test="@visibleIf and @visibleIf != '' ">
                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,') &amp;&amp; (', @visibleIf, ')')"
                    />
                </xsl:attribute>
                <xsl:attribute name="v-reset-when-hidden">
                    <xsl:value-of select="concat($apos, $fullName, $apos)" />
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>

                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')')"
                    />
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- ######## DESCRIPTION AND TOOLTIP ######## -->

    <xsl:template name="addVariableLabel">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="separatedTooltip">true</xsl:param>
        <xsl:param name="element">label</xsl:param>
        <xsl:param name="cssClass">variableLabel</xsl:param>

        <xsl:element name="{ $element }">
            <xsl:attribute name="class" select="$cssClass" />
            <xsl:if test="$fullName and ($fullName != '') and ($element = 'label')">
                <xsl:attribute name=":for">
                    <xsl:choose>
                        <xsl:when test="$withReferenceID and $withReferenceID != ''">
                            <xsl:value-of select="concat('getContextForID + ', $apos, $withReferenceID, $apos)" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat('getContextForID + ', $apos, $fullName, $apos)" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="$separatedTooltip and $separatedTooltip = 'true'">
                <xsl:call-template name="addSeparatedTooltip" />
            </xsl:if>

            <xsl:call-template name="getTranslatedLabel" />
        </xsl:element>

        <xsl:attribute name="v-on:mouseover.stop" select="concat('onHover(', $apos, $fullName, $apos, ')')" />
    </xsl:template>

    <xsl:template name="addSeparatedTooltip">
        <xsl:if test="@tooltip and @tooltip != ''">
            <xsl:variable name="escapedTooltip">
                <xsl:call-template name="escape">
                    <xsl:with-param name="string" select="@tooltip" />
                </xsl:call-template>
            </xsl:variable>
            <span><xsl:attribute name="tooltip" select="$escapedTooltip" />?</span>
        </xsl:if>
    </xsl:template>

    <xsl:template name="getTranslatedLabel">

        <xsl:variable name="text">
            <xsl:choose>
                <xsl:when test="@label and @label != ''">
                    <xsl:value-of select="@label" />
                </xsl:when>
                <xsl:when test="@name">
                    <xsl:value-of select="@name" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'-- this variable has no label or name! --'" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="translateString">
            <xsl:with-param name="string" select="$text" />
        </xsl:call-template>
    </xsl:template>

    <!-- ######## SIMPLE ATTRIBUTES ######## -->

    <xsl:template match="@precision">
        <xsl:variable name="pre" select="." />
        <xsl:attribute name="step">
            <xsl:if test="$pre != '' and $pre != 0">
                <xsl:value-of select="'0.'" />
            </xsl:if>
            <xsl:for-each select="1 to . -1">
                <xsl:value-of select="0" />
            </xsl:for-each>
            <xsl:value-of select="1" />
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@computedFormula">
        <xsl:call-template name="setComputedFormula">
            <xsl:with-param name="computedFormula" select="." />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="setComputedFormula">
        <xsl:param name="computedFormula" />
        <xsl:attribute name="v-short-circuit" select="$computedFormula" />
    </xsl:template>

    <xsl:template match="excludes">
        <xsl:call-template name="translateString">
            <xsl:with-param name="string">
                <xsl:value-of select="." />
            </xsl:with-param>
        </xsl:call-template>

        <xsl:if test="position() != last()">
            ,
        </xsl:if>
    </xsl:template>

    <!-- ######## ID ######## -->

    <xsl:template name="addWrapperID">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="IDattribute" />

        <xsl:call-template name="addID">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
            <xsl:with-param name="IDattribute" select="$IDattribute" />
            <xsl:with-param name="suffix" select="'-wrapper'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="addID">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="IDattribute" />
        <xsl:param name="suffix" select="''" />

        <xsl:variable name="nameForID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:if test="$IDattribute != ''">
                    <xsl:value-of select="concat($withReferenceID, '_' , $IDattribute)" />
                </xsl:if>
                <xsl:if test="$IDattribute = ''">
                    <xsl:value-of select="$withReferenceID" />
                </xsl:if>
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:if test="$IDattribute != ''">
                    <xsl:value-of select="concat($fullName, $IDattribute)" />
                </xsl:if>
                <xsl:if test="$IDattribute = ''">
                    <xsl:value-of select="$fullName" />
                </xsl:if>
            </xsl:if>
        </xsl:variable>

        <xsl:variable
            name="IDwithoutPrefix"
            select="concat('getContextForID + ' , $apos , $nameForID, $suffix, $apos)"
        />
        
        <xsl:if test="$prefixForID != ''">
            <xsl:attribute name=":id" select="concat($apos, $prefixForID, $apos, ' + ', $IDwithoutPrefix)" />
        </xsl:if>
        <xsl:if test="$prefixForID = ''">
            <xsl:attribute name=":id" select="$IDwithoutPrefix" />
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>

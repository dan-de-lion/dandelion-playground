﻿<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:v-on="vue.vue"
>

  <xsl:template match="variable[ @dataType = 'group' ] ">
    <xsl:param name="parent" />
    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>

  <xsl:template match="variable[ @dataType = 'text' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    
    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="parentName" select="$fullName" />
        <xsl:with-param name="parentDataClass" select="$parentDataClass" />
      </xsl:call-template>

      <xsl:call-template name="basicFilterOperators">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>

      <xsl:variable name="apos">'</xsl:variable>
        <input placeholder="first" v-show="getShowFilter('{$fullName}')">
          <xsl:attribute name=":value" select="concat('getFilterValue(', $apos, $fullName, $apos, ')')" />
          <xsl:attribute
                    name="v-on:input"
                    select="concat('v => setFilterValue(', $apos, $fullName, $apos, ', v?.target?.value)')"
                />
        </input>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'singlechoice' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="parentName" select="$fullName" />
        <xsl:with-param name="parentDataClass" select="$parentDataClass" />
      </xsl:call-template>

      <xsl:call-template name="basicFilterOperators">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>

      <xsl:variable name="apos">'</xsl:variable>
      <span v-show="getShowFilter('{$fullName}')"> 
        <!-- TODO: merge with CollectionbaseTypes -->
        <!-- TODO: maybe be fixed -->
        <select>
          <xsl:attribute name=":value" select="concat('getFilterValue(', $apos, $fullName, $apos, ')')" />
          <xsl:attribute
                        name="v-on:change"
                        select="concat('v => setFilterValue(', $apos, $fullName, $apos, ', v?.target?.value)')"
                    />
          <option value="" />
          <xsl:variable name="valuesSetName" select="@valuesSet" />
          <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
          <xsl:apply-templates select="value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
        </select>
      </span>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'multiplechoice' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="parentName" select="$fullName" />
        <xsl:with-param name="parentDataClass" select="$parentDataClass" />
      </xsl:call-template>

      <xsl:call-template name="setFilterOperators">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>

      <xsl:variable name="apos">'</xsl:variable>

      <span v-show="getShowFilter('{$fullName}')">
        <!-- TODO: merge with CollectionbaseTypes -->
        <select>
          <xsl:attribute name=":value" select="concat('getFilterValue(', $apos, $fullName, $apos, ')')" />
          <xsl:attribute
                        name="v-on:change"
                        select="concat('v => setFilterValue(', $apos, $fullName, $apos, ', v?.target?.value)')"
                    />
          <option value="" />
          <xsl:variable name="valuesSetName" select="@valuesSet" />
          <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
          <xsl:apply-templates select="value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
        </select>
      </span>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'date' or @dataType = 'time' or @dataType = 'number' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, @name)" />
    
    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="parentName" select="$fullName" />
        <xsl:with-param name="parentDataClass" select="$parentDataClass" />
      </xsl:call-template>

      <xsl:call-template name="filterOperatorsForSortable">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>

      <xsl:variable name="apos">'</xsl:variable>

      <div v-show="getShowFilter('{$fullName}')">
        <input type="{ @dataType }">
          <xsl:attribute name="v-on:input" select="concat('v => setLowerBound(', $apos, $fullName, $apos, ', v)')" />
          <xsl:apply-templates select="@precision" />
          <xsl:if test="@min">
            <xsl:attribute name="min" select="@min" />  
          </xsl:if>
          <xsl:if test="@max">
            <xsl:attribute name="max" select="@max" />
          </xsl:if>
        </input>
      </div>

      <xsl:variable name="apos">'</xsl:variable>
      <div>
        <xsl:attribute
                    name="v-show"
                    select="concat('(getShowFilter(', $apos, $fullName, $apos, ') &amp;&amp; getOperator(', $apos, $fullName, $apos, ') == ', $apos, 'between', $apos, ')')"
                />
        <input type="{ @dataType }">
        <xsl:attribute name="v-on:input" select="concat('v => setUpperBound(', $apos, $fullName, $apos, ', v)')" />
          <xsl:apply-templates select="@precision" />
          <xsl:if test="@min">
            <xsl:attribute name="min" select="@min" />
          </xsl:if>
          <xsl:if test="@max">
            <xsl:attribute name="max" select="@max" />
          </xsl:if>
        </input>
      </div>

      <div>
        <xsl:attribute
                    name="v-show"
                    select="concat('getUpperBound(', $apos, $fullName, $apos, ') &lt;= getLowerBound(', $apos, $fullName, $apos, ')')"
                />
        If lower bound is higher than upper bound, query may produce no result.
      </div>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'boolean'] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, @name)" />
    
    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="parentName" select="$fullName" />
        <xsl:with-param name="parentDataClass" select="$parentDataClass" />
      </xsl:call-template>

      <xsl:variable name="apos">'</xsl:variable>

      <div v-show="getShowFilter('{$fullName}')">
        <label>
          <input type="radio" value="true">
            <xsl:attribute name=":checked" select="concat('getFilterValue(', $apos, $fullName, $apos, ')')" />
            <xsl:attribute
                            name="v-on:input"
                            select="concat('v => setFilterValue(', $apos, $fullName, $apos, ', v?.target?.value == ''true'')')"
                        />
          </input>
          si
        </label>
        <label>
          <input type="radio" value="false">
            <xsl:attribute name=":checked" select="concat('getFilterValue(', $apos, $fullName, $apos, ') === false')" />
            <xsl:attribute
                            name="v-on:input"
                            select="concat('v => setFilterValue(', $apos, $fullName, $apos, ', v?.target?.value != ''false'')')"
                        />
          </input>
          no
        </label>
      </div>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="parentName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <!-- FILTER UTILS -->

  <xsl:template name="commonToAll">
    <xsl:param name="parentName" />
    <xsl:param name="parentDataClass" />

<!-- TODO, data-ng-init in vue -->
<!--    <xsl:attribute name="init-value" select="concat('(', $parentName, '.export === undefined) ? ', $parentName, '.export = ', $root, '.visibilityConfig.', $parentDataClass, '.', @name, ' : ', $parentName, '.export = ', $parentName, '.export')" />-->

    <!-- TODO
    <xsl:attribute name="data-ng-show">
      <xsl:value-of select="concat($parentName, '.showFilter || ', $parentName, '.export || showMore')"/>
    </xsl:attribute>
    -->

    <xsl:call-template name="addExportButton">
      <xsl:with-param name="parentName" select="$parentName" />
    </xsl:call-template>
    <xsl:call-template name="addVariableLabel">
      <xsl:with-param name="fullName" select="$parentName" />
    </xsl:call-template>
    <xsl:call-template name="addFilterButton">
      <xsl:with-param name="parentName" select="$parentName" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="addExportButton">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <button type="button" class="export-button">
      <xsl:attribute name="v-on:click.stop" select="concat('setToBeExported(', $apos, $parentName, $apos, ')')" />
      <xsl:attribute
                name=":class"
                select="concat('{''icon-eye-plus'': ! getToBeExported(', $apos, $parentName, $apos, '), ''unselected'': ! getToBeExported(', $apos, $parentName, $apos, '), ''icon-eye-minus'': getToBeExported(', $apos, $parentName, $apos, ')', '}')"
            />
    </button>
  </xsl:template>

  <xsl:template name="addFilterButton">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <button type="button" class="icon-filter">
      <xsl:attribute name="v-on:click.stop" select="concat('setShowFilter(', $apos, $parentName, $apos, ')')" />
      <xsl:attribute
                name=":class"
                select="concat('{''unselected'': ! getShowFilter(', $apos, $parentName, $apos, ')', '}')"
            />
    </button>
  </xsl:template>

  <xsl:template name="addRemoveFilterButton">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <button type="button" class="icon-cross" v-show="getShowFilter('{$parentName}')">
      <xsl:attribute name="v-on:click.stop" select="concat('setShowFilter(', $apos, $parentName, $apos, ', false)')" />
    </button>
  </xsl:template>

  <!-- TODO: merge with CollectionbaseTypes -->
  <xsl:template match="value" mode="select">
    <xsl:param name="valuesSetName" />
    <option value="{ concat($valuesSetName, '.', @name) }">
      <xsl:call-template name="getTranslatedLabel" />
    </option>
  </xsl:template>

  <!-- OPERATORS -->

  <xsl:template name="filterOperatorsForSortable">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <!-- TODO, data-ng-init in vue -->
    <select v-if="getShowFilter('{$parentName}')">
      <xsl:attribute name=":value" select="concat('getOperator(', $apos, $parentName, $apos, ')')" />
      <xsl:attribute
                name="v-on:change"
                select="concat('v => setOperator(', $apos, $parentName, $apos, ', v?.target?.value)')"
            />

      <option value="" />
      <option value="=">uguale a</option>
      <option value="&gt;">maggiore di</option>
      <option value="&lt;">minore di</option>
      <option value="between">compreso tra</option>
      <option value="!=">diverso da</option>
    </select>
  </xsl:template>

  <xsl:template name="basicFilterOperators">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <!-- TODO, data-ng-init in vue -->
    <select v-if="getShowFilter('{$parentName}')">
      <xsl:attribute name=":value" select="concat('getOperator(', $apos, $parentName, $apos, ')')" />
      <xsl:attribute
                name="v-on:change"
                select="concat('v => setOperator(', $apos, $parentName, $apos, ', v?.target?.value)')"
            />
   
      <option value="" />
      <option value="=">uguale a</option>
      <option value="!=">diverso da</option>
    </select>
  </xsl:template>

  <xsl:template name="setFilterOperators">
    <xsl:param name="parentName" />
    <xsl:variable name="apos">'</xsl:variable>
    <!-- TODO, data-ng-init in vue -->
    <select v-if="getShowFilter('{$parentName}')">
      <xsl:attribute name=":value" select="concat('getOperator(', $apos, $parentName, $apos, ')')" />
      <xsl:attribute
                name="v-on:change"
                select="concat('v => setOperator(', $apos, $parentName, $apos, ', v?.target?.value)')"
            />
   
      <option value="contains" selected="selected">contiene</option>
      <option value="notContains">non contiene</option>
    </select>
  </xsl:template>

</xsl:stylesheet>

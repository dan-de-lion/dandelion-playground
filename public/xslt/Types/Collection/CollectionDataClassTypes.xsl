<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## GROUP ######## -->
    <!-- container for other variables -->

    <xsl:template match="variable [ @dataType = 'group' and not(@dataClass) ] ">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextParent" select="concat($fullName, '.')" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="meIsAPage">
            <xsl:if test="@interface = 'pagelink' or $meIsAPageFromParent = 'true'">
                <xsl:value-of select="true()" />
            </xsl:if>
            <xsl:if test="(not(@interface) or @interface != 'pagelink') and $meIsAPageFromParent = 'false'">
                <xsl:value-of select="false()" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.BeforeVariable)</xsl:variable>
        <component>
            <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
            <xsl:attribute name=":is">componentName</xsl:attribute>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
        </component>

        <xsl:if test="@interface = 'pagelink'">
            <a class="page-link">
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                />
                <xsl:call-template name="applyVisibility">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
                <xsl:call-template name="getTranslatedLabel" />
            </a>
        </xsl:if>

        <fieldset v-custom-model="'{$fullName}'">
            <xsl:attribute name=":disabled">
                <xsl:choose>
                    <xsl:when test="@computedFormula and @computedFormula != '' ">
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="@computedFormula" />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:attribute>
            </xsl:if>

            <xsl:attribute name="class" select="@name" />

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssClass">groupDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:if test="$statisticianMode = 'true'">
                <span class="statistician" v-html="'{$fullName}'" />
            </xsl:if>

            <xsl:if test="$meIsAPage = true()">
                <xsl:attribute
                    name="v-show"
                    select="concat('listOfActivePages.includes(getContextForID + ', $apos, $fullName, $apos, ')')"
                />
            </xsl:if>

            <xsl:if test="@separateInPages">
                <nav>
                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="./variable" mode="nav">
                                <xsl:with-param name="parent" select="$nextParent" />
                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="./variable" mode="nav">
                                <xsl:with-param name="parent" select="$nextParent" />
                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="./variable" mode="nav">
                                <xsl:with-param name="parent" select="$nextParent" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                </nav>
            </xsl:if>

            <div>
                <xsl:variable name="separateChildrenInPages">
                    <xsl:choose>
                        <xsl:when test="@separateInPages">
                            <xsl:value-of select="true()" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="false()" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:choose>
                    <xsl:when test="@requiredForStatus">
                        <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="$nextParent" />
                            <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                            <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                            <xsl:with-param name="setLevel" select="$setLevel" />
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="$nextParent" />
                            <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            <xsl:with-param name="setLevel" select="$setLevel" />
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="$nextParent" />
                            <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                            <xsl:with-param name="setLevel" select="$setLevel" />
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
            </div>

            <xsl:apply-templates select="./error">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./warning">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./requirement">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </fieldset>
    </xsl:template>

    <!-- ######## DATACLASS ######## -->
    <!-- same as a "group" of variables, but can be inserted in different points of the data
    collection -->
    <xsl:template match="dataClass">
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="parent" />
        <xsl:param name="separateChildrenInPages" />
        <xsl:param name="arrayOfExcludes" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:param name="withReferenceID" />

        <!-- fullName is chosen differently if it's the first dataclass -->
        <xsl:variable name="fullName">
            <xsl:call-template name="selectFullName">
                <xsl:with-param name="fullName" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <!-- parent is chosen differently if it's the first dataclass -->
        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <!-- chooses if we should separate children given -->
        <xsl:variable name="shouldSeparateInPages">
            <xsl:choose>
                <xsl:when test="$separateChildrenInPages = 'true'">
                    <!-- value from instance variable takes precedence -->
                    <xsl:value-of select="$separateChildrenInPages" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@separateInPages" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <DataclassItemComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            <xsl:attribute name=":this-index">
                <xsl:if test="$setLevel != 0">
                    <xsl:value-of select="concat('index', $setLevel - 1)" />
                </xsl:if>
                <xsl:if test="$setLevel = 0">null</xsl:if>
            </xsl:attribute>
            <xsl:attribute name=":model">model</xsl:attribute>
            <xsl:attribute name=":this-set">thisSet</xsl:attribute>

            <xsl:element name="my-custom-template">
                <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />

                <xsl:if test="$shouldSeparateInPages = 'true'">
                    <nav>
                        <xsl:apply-templates select="./variable" mode="nav">
                            <xsl:with-param name="parent" select="$selectedParent" />
                            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                        </xsl:apply-templates>
                    </nav>
                </xsl:if>

                <section>
                    <xsl:if test="@activateIf">
                        <xsl:attribute name="v-if" select="@activateIf" />
                    </xsl:if>

                    <xsl:if test="@requiredForStatus">
                        <xsl:attribute name="required-for-status">
                            <xsl:value-of select="@requiredForStatus" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                        <xsl:attribute name="required-for-status">
                            <xsl:value-of select="$requiredFromParent" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:attribute name="class" select="@name" />

                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="./variable">
                                <xsl:with-param name="setLevel" select="$setLevel" />
                                <xsl:with-param name="parent" select="$selectedParent" />
                                <xsl:with-param name="meIsAPageFromParent" select="$shouldSeparateInPages" />
                                <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="./variable">
                                <xsl:with-param name="setLevel" select="$setLevel" />
                                <xsl:with-param name="parent" select="$selectedParent" />
                                <xsl:with-param name="meIsAPageFromParent" select="$shouldSeparateInPages" />
                                <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="./variable">
                                <xsl:with-param name="setLevel" select="$setLevel" />
                                <xsl:with-param name="parent" select="$selectedParent" />
                                <xsl:with-param name="meIsAPageFromParent" select="$shouldSeparateInPages" />
                                <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:apply-templates select="./error">
                        <xsl:with-param name="parentName" select="$selectedParent" />
                        <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                    </xsl:apply-templates>
                    <xsl:apply-templates select="./warning">
                        <xsl:with-param name="parentName" select="$selectedParent" />
                        <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                    </xsl:apply-templates>
                    <xsl:apply-templates select="./requirement">
                        <xsl:with-param name="parentName" select="$selectedParent" />
                        <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                    </xsl:apply-templates>
                </section>

                <xsl:if test="@activateIf">
                    <section>
                        <xsl:attribute name="v-if" select="concat('!', @activateIf)" />
                        <output
                            v-custom-model-for-requirements="'{concat($selectedParent, 'ACTIVE')}'"
                            required-for-status="1"
                        />
                        <h3>
                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="concat(@name, ' not active')" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </h3>
                        <p>
                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="@howToActivate" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </p>
                    </section>
                </xsl:if>
            </xsl:element>
        </DataclassItemComponent>
    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '']">
        <xsl:param name="setLevel" />
        <xsl:param name="parent" />
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="withReferenceID" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="meIsAPage">
            <xsl:if test="@interface = 'pagelink' or $meIsAPageFromParent = 'true'">
                <xsl:value-of select="true()" />
            </xsl:if>
            <xsl:if test="(not(@interface) or @interface != 'pagelink') and $meIsAPageFromParent = 'false'">
                <xsl:value-of select="false()" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="dataClass" select="@dataClass" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:if test="@interface = 'pagelink'">
            <a class="page-link">
                <xsl:attribute
                    name="v-on:click"
                    select="concat('currentPageService.set(getContextForID + ', $apos, $fullName, $apos, ')')"
                />
                <xsl:call-template name="applyVisibility">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
                <xsl:call-template name="getTranslatedLabel" />
            </a>
        </xsl:if>

        <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.BeforeVariable)</xsl:variable>
        <component>
            <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
            <xsl:attribute name=":is">componentName</xsl:attribute>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
        </component>

        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="requiredForStatus">
            <xsl:choose>
                <xsl:when test="@requiredForStatus">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:when>
                <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:when>
                <xsl:otherwise />
            </xsl:choose>
        </xsl:variable>

        <fieldset v-custom-model="'{$fullName}'" v-if="userService.canRead('{$fullName}','{$crfName}','{$crfVersion}')">
            <xsl:if test="$requiredForStatus != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:attribute name="class" select="@name" />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <xsl:attribute name=":disabled">
                <xsl:choose>
                    <xsl:when test="@computedFormula and @computedFormula != ''">
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>

            <xsl:apply-templates select="@computedFormula" />

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssClass">groupDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:if test="$meIsAPage = true()">
                <xsl:attribute
                    name="v-show"
                    select="concat('listOfActivePages.includes(getContextForID + ', $apos, $fullName, $apos,')')"
                />
            </xsl:if>

            <xsl:apply-templates select="//dataClass [ @name = $dataClass ]">
                <xsl:with-param name="setLevel" select="$setLevel" />
                <xsl:with-param name="parent" select="$fullName" />
                <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages" />
                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                <xsl:with-param name="meIsAPage" select="$meIsAPage" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>

            <xsl:apply-templates select="./error">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./warning">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./requirement">
                <xsl:with-param name="parentName" select="$nextParent" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:apply-templates>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterVariable)</xsl:variable>

            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>

        </fieldset>
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeNavNotDefined" mode="nav" />

</xsl:stylesheet>

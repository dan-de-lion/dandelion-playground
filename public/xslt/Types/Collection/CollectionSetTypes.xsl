<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'set' ]">
        <xsl:param name="parent" />
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="dataClass" select="@itemsDataClass" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nameWithIndex">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="@name" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', $nameWithIndex)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.BeforeVariable)</xsl:variable>
        <component>
            <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
            <xsl:attribute name=":is">componentName</xsl:attribute>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
        </component>

        <xsl:variable name="addElementClickAction">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of
                        select="concat('addElementToSet(thisSet); currentPageService.set(getContextForID + ', $apos, $fullName, '[', $apos, ' + (thisSet.length - 1) + ', $apos, ']', $apos, ')')"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'addElementToSet(thisSet)'" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="elementName">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>

        <fieldset v-custom-model-for-sets="'{$fullName}'" init-value="[]">
            <xsl:if test="@initValue">
                <xsl:attribute name="init-value">
                    <xsl:value-of select="@initValue" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:attribute>
            </xsl:if>

            <xsl:attribute name=":disabled">
                <xsl:choose>
                    <xsl:when test="@computedFormula and @computedFormula != '' ">
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of
                            select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates select="@computedFormula" />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
            </xsl:call-template>

            <xsl:attribute name="class" select="@name" />

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssClass">setDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:apply-templates select="@computedItems" />

            <xsl:if test="$meIsAPageFromParent = true()">
                <xsl:attribute
                    name="v-show"
                    select="concat('listOfActivePages.includes(getContextForID + ', $apos, $fullName, $apos, ')')"
                />
            </xsl:if>

            <SetComponent>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName , $apos)" />
                <xsl:attribute name=":this-object">thisObject</xsl:attribute>
                <xsl:attribute name=":this-index">
                    <xsl:if test="$setLevel != 0"><xsl:value-of select="concat('index', $setLevel - 1)" /></xsl:if>
                    <xsl:if test="$setLevel = 0">null</xsl:if>
                </xsl:attribute>
                <xsl:attribute name=":model">model</xsl:attribute>

                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />
                    <xsl:apply-templates select="./description" />

                    <xsl:if test="@separateInPages">
                        <section class="itemSelector">
                            <button type="button" class="add-item">
                                <xsl:attribute name="v-on:click.stop" select="$addElementClickAction" />
                                <xsl:attribute
                                    name=":disabled"
                                    select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                                />
                                <xsl:call-template name="translateString">
                                    <xsl:with-param name="string">
                                        <xsl:value-of select="concat('Add ', @name)" />
                                    </xsl:with-param>
                                </xsl:call-template>
                            </button>
                            <nav>
                                <xsl:variable
                                    name="itemLabelVariable"
                                    select="//dataClass[ @name = $dataClass ]/variable[ @itemLabel ]/@name"
                                />
                                <button
                                    type="button"
                                    class="page-navigation"
                                    v-for="{ concat('(childElement, index', $setLevel, ') in thisSet') }"
                                >
                                    <xsl:attribute name=":key">childElement.guid</xsl:attribute>
                                    <xsl:attribute
                                        name="v-on:click.stop"
                                        select="concat('currentPageService.set(getContextForID + ', $apos, $elementName, $apos, ')')"
                                    />
                                    <xsl:attribute
                                        name=":class"
                                        select="concat('{selected: ', concat('listOfActivePages.includes(getContextForID + ', $apos, $elementName, $apos, ')'), '}')"
                                    />
                                    <xsl:attribute name="v-html">
                                        <xsl:if test="$itemLabelVariable">
                                            <xsl:value-of
                                                select="concat('(childElement.', $itemLabelVariable, '?.getLabel?.() ?? childElement.', $itemLabelVariable,
                                                ') || translationService.t(', $apos, '-- new ', @name, ' --', $apos, ')')"
                                            />
                                        </xsl:if>
                                        <xsl:if test="not($itemLabelVariable)">
                                            <xsl:value-of
                                                select="concat('translationService.t(', $apos, '-- new ', @name, ' --', $apos, ')')"
                                            />
                                        </xsl:if>
                                    </xsl:attribute>
                                    <!--translationService.t() and xsl:call-template name="translateString don't work, find a solution" >-->
                                </button>
                            </nav>
                        </section>
                    </xsl:if>

                    <ul>
                        <xsl:call-template name="setItem">
                            <xsl:with-param name="elementName" select="$elementName" />
                            <xsl:with-param name="setLevel" select="$setLevel" />
                            <xsl:with-param name="dataClass" select="$dataClass" />
                            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                        </xsl:call-template>
                    </ul>

                    <xsl:if test="not(@separateInPages) and not(@computedItems)">
                        <button type="button" class="add-item">
                            <xsl:attribute name="v-on:click.stop">addElementToSet(thisSet)</xsl:attribute>
                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="concat('Add ', $dataClass)" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </button>
                    </xsl:if>

                    <xsl:call-template name="afterVariable">
                        <xsl:with-param name="fullName" select="$fullName" />
                        <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    </xsl:call-template>
                </xsl:element>
            </SetComponent>
        </fieldset>
    </xsl:template>

    <xsl:template match="@computedItems">
        <xsl:variable name="itemsDataClass" select="../@itemsDataClass" />
        <xsl:attribute name="v-computed-items" select="." />
        <xsl:attribute name="item-key" select="//dataClass[ @name = $itemsDataClass ]/variable[ @itemKey ]/@name" />
    </xsl:template>

    <xsl:template name="setItem">
        <xsl:param name="elementName" />
        <xsl:param name="setLevel" />
        <xsl:param name="dataClass" />
        <xsl:param name="requiredFromParent" />
        <xsl:param name="withReferenceID" />

        <li v-for="{ concat('(childElement, index', $setLevel, ') in thisSet') }">
            <xsl:attribute name=":key">childElement.guid</xsl:attribute>

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$elementName" />
            </xsl:call-template>

            <xsl:if test="@separateInPages">
                <xsl:attribute
                    name="v-show"
                    select="concat('listOfActivePages.includes(getContextForID + ', $apos, $elementName, $apos, ')')"
                />
            </xsl:if>

            <SetItemComponent>
                <xsl:attribute name=":full-name" select="concat($apos, $elementName, $apos)" />
                <xsl:attribute name=":this-set">thisSet</xsl:attribute>
                <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
                <xsl:attribute name=":model">model</xsl:attribute>
                <xsl:attribute name=":key" select="concat('index', $setLevel)" />

                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />
                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                <xsl:with-param name="parent" select="$elementName" />
                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                <xsl:with-param name="parent" select="$elementName" />
                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                <xsl:with-param name="parent" select="$elementName" />
                                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="not(@computedItems)">
                        <button type="button" class="remove-item">
                            <xsl:attribute
                                name="v-on:click.stop"
                                select="concat('removeElementFromSet(thisSet, index', $setLevel, ')')"
                            />
                            <xsl:attribute
                                name=":disabled"
                                select="concat('!userService?.canWrite(', $apos, $elementName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="concat('Remove this ', @name)" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </button>
                    </xsl:if>
                </xsl:element>
            </SetItemComponent>
        </li>
    </xsl:template>
</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'reference' and (not(@interface) or @interface != 'pagelink') ]">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="referenceToBePassed">
            <xsl:if test="$cleanedRoot = ''">
                <xsl:value-of select="@referencedFullName" />
            </xsl:if>
            <xsl:if test="$cleanedRoot != ''">
                <xsl:value-of select="replace(@referencedFullName, concat($cleanedRoot, '.'), '')" />
            </xsl:if>
        </xsl:variable>
        <!--
            References to don't follow the startingDataClass of the component in which they are,
            they have a separate startingDataClass attribute because they should always
            refer to the same exact variable, so it's up to them to choose the starting point
        -->
        <xsl:if test="@startingDataClass != ''">
            <xsl:variable name="referenceStartingDataClass" select="@startingDataClass" />
            <xsl:apply-templates
                select="/root/dataClass[@name = $referenceStartingDataClass]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$referenceToBePassed" />
                <xsl:with-param name="cleanedRoot" select="$cleanedRoot" />
                <xsl:with-param name="parentName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test=" not(@startingDataClass) or @startingDataClass = ''">
            <xsl:apply-templates select="/root/dataClass[1]" mode="referenceResolution">
                <xsl:with-param name="variableReference" select="$referenceToBePassed" />
                <xsl:with-param name="cleanedRoot" select="$cleanedRoot" />
                <xsl:with-param name="parentName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'reference' and @interface = 'pagelink' ]">
        <xsl:param name="parent" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="referenceToBePassed">
            <xsl:if test="$cleanedRoot = ''">
                <xsl:value-of select="@referencedFullName" />
            </xsl:if>
            <xsl:if test="$cleanedRoot != ''">
                <xsl:value-of select="replace(@referencedFullName, concat($cleanedRoot, '.'), '')" />
            </xsl:if>
        </xsl:variable>

        <a class="page-link">
            <xsl:attribute
                name="v-on:click.stop"
                select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, @referencedFullName, $apos), ')')"
            />
            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
            <xsl:apply-templates select="/root/dataClass[1]" mode="labelResolution">
                <xsl:with-param name="variableReference" select="$referenceToBePassed" />
            </xsl:apply-templates>
        </a>
    </xsl:template>

    <xsl:template match="variable" mode="referenceResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="cleanedRoot" />
        <xsl:param name="parentName" />
        <xsl:param name="setLevel" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="parentToBePassed">
            <xsl:if test="$cleanedRoot = ''">
                <xsl:value-of select="$cleanedRoot" />
            </xsl:if>
            <xsl:if test="$cleanedRoot != ''">
                <xsl:value-of select="concat($cleanedRoot, '.')" />
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$rest = ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]">
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>

            <xsl:variable name="dataClass" select="@dataClass" />

            <xsl:apply-templates select="//dataClass[@name = $dataClass]/variable[@name=$variableToFind]">
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$rest != ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]" mode="referenceResolution">
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="cleanedRoot" select="concat($parentToBePassed, $variableToFind)" />
                <xsl:with-param name="parentName" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>

            <xsl:variable name="dataClass" select="@dataClass" />

            <xsl:apply-templates
                select="//dataClass[@name = $dataClass]/variable[@name=$variableToFind]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="cleanedRoot" select="concat($parentToBePassed, $variableToFind)" />
                <xsl:with-param name="parentName" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="dataClass" mode="referenceResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="cleanedRoot" />
        <xsl:param name="parentName" />
        <xsl:param name="setLevel" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="parentToBePassed">
            <xsl:if test="$cleanedRoot = ''">
                <xsl:value-of select="$cleanedRoot" />
            </xsl:if>
            <xsl:if test="$cleanedRoot != ''">
                <xsl:value-of select="concat($cleanedRoot, '.')" />
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$rest = ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]">
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$rest != ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]" mode="referenceResolution">
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="cleanedRoot" select="concat($parentToBePassed, $variableToFind)" />
                <xsl:with-param name="parentName" select="$parentName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable | dataClass" mode="labelResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="setLevel" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="$variableToFind = ''">
            <xsl:call-template name="getTranslatedLabel" />
        </xsl:if>
        <xsl:if test="$variableToFind != ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]" mode="labelResolution">
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>

            <xsl:variable name="dataClass" select="@dataClass" />

            <xsl:apply-templates
                select="//dataClass[@name = $dataClass]/variable[@name = $variableToFind]"
                mode="labelResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template name="firstChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:choose>
            <xsl:when test="contains($fullNameToTrim, '.')">
                <xsl:value-of select="substring-before($fullNameToTrim, '.')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fullNameToTrim" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="restChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:choose>
            <xsl:when test="contains($fullNameToTrim, '.')">
                <xsl:value-of select="substring-after($fullNameToTrim, '.')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template mode="referenceLabel" match="*">
        <xsl:call-template name="getTranslatedLabel" />
    </xsl:template>

</xsl:stylesheet>

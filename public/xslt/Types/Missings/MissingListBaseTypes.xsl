<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable [ @dataType= 'group' ] ">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="missingName" select="concat( $fullName, '.MISSING')" />

        <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
            <li
                class="requirement"
                v-show="isVariableMissing('{ $fullName }', null) &amp;&amp; userService?.canRead('{$fullName}', '{$crfName}', '{$crfVersion}')"
            >
                <xsl:attribute
                    name=":class"
                    select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos, ')'), '}')"
                />
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                />
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$missingName" />
                </xsl:call-template>            
            </li>
        </xsl:if>

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="parentName" select="concat($fullName, '.')" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />
        <xsl:variable name="dataClass" select="@itemsDataClass" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>

        <xsl:variable name="elementName">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>

        <SetComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName , $apos)" />
            <xsl:attribute name=":this-object">thisObject</xsl:attribute>
            <xsl:attribute name=":this-index">
                <xsl:if test="$setLevel != 0"><xsl:value-of select="concat('index', $setLevel - 1)" /></xsl:if>
                <xsl:if test="$setLevel = 0">null</xsl:if>
            </xsl:attribute>
            <xsl:attribute name=":model">model</xsl:attribute>
            <xsl:element name="my-custom-template">
                <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }</xsl:attribute>
                <SetItemComponent>
                    <xsl:attribute name="v-for" select="concat('(childElement, index', $setLevel, ') in thisSet')" />
                    <xsl:attribute name=":full-name" select="concat($apos, $elementName , $apos)" />
                    <xsl:attribute name=":this-set">thisSet</xsl:attribute>
                    <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
                    <xsl:attribute name=":model">model</xsl:attribute>
                    <xsl:element name="my-custom-template">
                        <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />
                        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                            <xsl:with-param name="setLevel" select="$setLevel + 1" />
                            <xsl:with-param name="parent" select="$elementName" />
                        </xsl:apply-templates>
                    </xsl:element>
                </SetItemComponent>
            </xsl:element>
        </SetComponent>

        <xsl:apply-templates select="requirement">
            <xsl:with-param name="parentName" select="concat($fullName, '_')" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable">
        <xsl:param name="parent" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="missingName" select="concat($fullName, '_MISSING')" />

        <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
            <li
                class="requirement"
                v-show="isVariableMissing('{$fullName}', null) &amp;&amp; userService?.canRead('{$fullName}', '{$crfName}', '{$crfVersion}')"
            >
                <xsl:attribute
                    name=":class"
                    select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
                />
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                />
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$missingName" />
                </xsl:call-template>            
                <xsl:call-template name="getTranslatedLabel" />
            </li>
        </xsl:if>
        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="parentName" select="concat($fullName, '_')" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="requirement">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:call-template name="createRequirement">
            <xsl:with-param name="parentName" select="$parentName" />
            <xsl:with-param name="requirementMessage" select="." />
            <xsl:with-param name="satisfiedIfCondition" select="@satisfiedIf" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template
        match="variable[ @dataType = 'number' or  @dataType = 'text' or @dataType = 'email' or @dataType = 'color' or @dataType = 'time' or @dataType = 'date' or @dataType = 'singlechoice' or  @dataType = 'plugin' or @dataType = 'output' or @dataType = 'boolean']"
    >
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="protectedFullName">
            <xsl:call-template name="protectFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="newModel" select="concat('model?.newModel?.', $protectedFullName)" />
        <xsl:variable name="oldModel" select="concat('model?.oldModel?.', $protectedFullName)" />

        <xsl:variable name="newVariable">
            <xsl:choose>
                <xsl:when test="@dataType = 'singlechoice'">
                    <xsl:value-of select="concat('convertValuesSet(', $newModel , ')')" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$newModel" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="oldVariable">
            <xsl:choose>
                <xsl:when test="@dataType = 'singlechoice'">
                    <xsl:value-of select="concat('convertValuesSet(', $oldModel , ')')" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$oldModel" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div v-if="!({$oldVariable}) &amp;&amp; ({$newVariable})" class="added">
            <xsl:variable name="message">The variable has been added, the current value is:</xsl:variable>
            <p class="variable-diffs">
                <xsl:value-of select="concat(@name, ': ')" />
            </p>
            <p class="message">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="concat('translationService.t(''', $message, ''')')" />
                </xsl:attribute>
            </p>
            <p class="newVariable">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="$newVariable" />
                </xsl:attribute>
            </p>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>
        <div v-else-if="!({$newVariable}) &amp;&amp; ({$oldVariable})" class="removed">
            <xsl:variable name="message">The variable has been removed, the old value is:</xsl:variable>
            <p class="variable-diffs">
                <xsl:value-of select="concat(@name, ': ')" />
            </p>
            <p class="message">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="concat('translationService.t(''', $message, ''')')" />
                </xsl:attribute>
            </p>
            <p class="oldVariable">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="$oldVariable" />
                </xsl:attribute>
            </p>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>
        <div
            v-else-if="({$newVariable}) &amp;&amp; ({$oldVariable}) &amp;&amp; ({$newVariable}) != ({$oldVariable})"
            class="modified"
        >
            <xsl:variable name="message">The variable has been modified:</xsl:variable>
            <p class="variable-diffs">
                <xsl:value-of select="concat(@name, ': ')" />
            </p>
            <p class="message">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="concat('translationService.t(''', $message, ''')')" />
                </xsl:attribute>
            </p>
            <p class="model-values">
                <span v-html="{$oldVariable}" />
                <span>&#10132;</span>
                <span v-html="{$newVariable}" />
            </p>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>
        <!-- v-else should be declared in this way because without equal gives error -->
        <div v-else-if="true" class="unchanged">
            <xsl:variable name="message">The variable has not been modified</xsl:variable>
            <p class="variable-diffs">
                <xsl:value-of select="concat(@name, ': ')" />
            </p>
            <p class="message">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="concat('translationService.t(''', $message, ''')')" />
                </xsl:attribute>
            </p>

            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType= 'multiplechoice']">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="protectedFullName">
            <xsl:call-template name="protectFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="newModel" select="concat('model?.newModel?.', $protectedFullName)" />
        <xsl:variable name="oldModel" select="concat('model?.oldModel?.', $protectedFullName)" />

        <div class="differences-multiplechoice">
            <p class="variable-diffs">
                <xsl:value-of select="concat(@name, ': ')" />
            </p>
            <div class="variable-diffs-value">
                <xsl:attribute name="v-html">
                    <xsl:value-of select="concat('compareArray(', $oldModel , ', ', $newModel, ')')" />
                </xsl:attribute>
            </div>
            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>

    </xsl:template>

</xsl:stylesheet>

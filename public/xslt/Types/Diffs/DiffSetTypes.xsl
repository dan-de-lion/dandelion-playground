<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'set' ]">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="protectedFullName">
            <xsl:call-template name="protectFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="newModel" select="concat('model?.newModel?.', $protectedFullName)" />
        <xsl:variable name="oldModel" select="concat('model?.oldModel?.', $protectedFullName)" />

        <div>
            <xsl:attribute name="v-html">
                <xsl:value-of select="concat('compareSets(', $oldModel , ', ', $newModel, ')')" />
            </xsl:attribute>
        </div>

        <xsl:variable name="mergedModels">
            <xsl:value-of select="concat('mergeModels(', $oldModel , ', ', $newModel, ')')" />
        </xsl:variable>

        <div v-for="childElement in {$mergedModels}">
            <DiffListComponent starting-dataclass="{@itemsDataClass}">
                <xsl:attribute name=":model">childElement.newModel</xsl:attribute>
                <xsl:attribute name=":model-to-compare">childElement.oldModel</xsl:attribute>
            </DiffListComponent>
            <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
            <component>
                <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                <xsl:attribute name=":is">componentName</xsl:attribute>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </component>
        </div>
    </xsl:template>

</xsl:stylesheet>

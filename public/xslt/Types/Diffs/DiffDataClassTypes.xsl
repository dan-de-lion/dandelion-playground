<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## GROUP ######## -->
    <!-- container for other variables -->

    <xsl:template match="variable [ @dataType = 'group' and not(@dataClass) ] ">
        <xsl:param name="parent" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <fieldset>
            <xsl:attribute name="class" select="@name" />

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssClass">groupDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:variable name="nextParent" select="concat($fullName, '.')" />

            <div>
                <xsl:apply-templates select="./variable">
                    <xsl:with-param name="parent" select="$nextParent" />
                    <xsl:with-param name="requiredFromParent">
                        <xsl:choose>
                            <xsl:when test="@requiredForStatus">
                                <xsl:value-of select="@requiredForStatus" />
                            </xsl:when>
                            <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                                <xsl:value-of select="$requiredFromParent" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$requiredFromParent" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                </xsl:apply-templates>
                <xsl:variable name="getHooksByType">getHooksByType(TypesOfHooks.AfterChangedVariable)</xsl:variable>
                <component>
                    <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
                    <xsl:attribute name=":is">componentName</xsl:attribute>
                    <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
                </component>
            </div>
        </fieldset>
    </xsl:template>

    <!-- ######## DATACLASS ######## -->
    <!-- same as a "group" of variables, but can be inserted in different points of the data
    collection -->
    <xsl:template match="dataClass">
        <xsl:param name="setLevel">0</xsl:param>

        <!-- (use this for $arrayOfExcludes) -->
        <!-- to identify itself (use this for $arrayOfExcludes) -->
        <xsl:param name="parent" />
        <xsl:param name="arrayOfExcludes" />
        <xsl:param name="requiredFromParent" select="''" />

        <section>
            <xsl:if test="@activateIf">
                <xsl:attribute name="v-if" select="@activateIf" />
            </xsl:if>

            <xsl:variable name="selectedParent">
                <xsl:choose>
                    <xsl:when test="not($parent) and $cleanedRoot != ''">
                        <xsl:value-of select="concat($cleanedRoot, '.')" />
                    </xsl:when>
                    <xsl:when test="not($parent) and $cleanedRoot = ''">
                        <xsl:value-of select="$cleanedRoot" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($parent, '.')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:attribute name="class">
                <xsl:value-of select="@name" />
            </xsl:attribute>


            <xsl:apply-templates select="./variable">
                <xsl:with-param name="setLevel" select="$setLevel" />
                <xsl:with-param name="parent" select="$selectedParent" />
                <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                <xsl:with-param name="requiredFromParent">
                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:value-of select="@requiredForStatus" />
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:value-of select="$requiredFromParent" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$requiredFromParent" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
            </xsl:apply-templates>

        </section>

    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '']">
        <xsl:param name="parent" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="dataClass" select="@dataClass" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:call-template name="addVariableLabel">
            <xsl:with-param name="element">legend</xsl:with-param>
            <xsl:with-param name="cssClass">groupDescription</xsl:with-param>
        </xsl:call-template>

        <xsl:apply-templates select="//dataClass [ @name = $dataClass ]">
            <xsl:with-param name="parent" select="$fullName" />
            <xsl:with-param name="requiredFromParent">
                <xsl:choose>
                    <xsl:when test="@requiredForStatus">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:when>
                    <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeNavNotDefined" mode="nav" />

</xsl:stylesheet>

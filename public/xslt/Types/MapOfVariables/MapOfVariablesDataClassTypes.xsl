<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">

        <xsl:param name="parent" />

        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>

    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable">
        <xsl:param name="parent" />
        <xsl:variable name="dataType" select="@dataType" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        {
            "fullName" : "<xsl:value-of select="concat($root, '.', $fullName)" />",
            "type" : "<xsl:value-of select="$dataType" />"
            <xsl:if test="(($dataType = 'singlechoice') or ($dataType = 'multiplechoice')) and @valuesSet">
                ,
                "valuesSet" : "<xsl:value-of select="@valuesSet" />"
            </xsl:if>
            <xsl:if test="(($dataType = 'singlechoice') or ($dataType = 'multiplechoice')) and not(@valuesSet)">
                ,
                "valuesSet" : "<xsl:value-of select="@name" />"
            </xsl:if>
        } ,
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>
    </xsl:template>
</xsl:stylesheet>

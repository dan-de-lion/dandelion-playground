<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">
        <xsl:param name="parent" />
        <xsl:param name="depthLevel">0</xsl:param>
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="separateChildrenFromParent" />

        <xsl:variable name="cleanedRoot">
            <xsl:call-template name="createCleanedRoot" />
        </xsl:variable>

        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages or $separateChildrenFromParent = true()">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- fullName is chosen differently if it's the first dataclass -->
        <xsl:variable name="fullName">
            <xsl:call-template name="selectFullName">
                <xsl:with-param name="fullName" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <!-- selectedParent is chosen differently if it's the first dataclass -->
        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <DataclassItemComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName , $apos)" />
            <xsl:attribute name=":model">model</xsl:attribute>
            <xsl:attribute name=":this-index">
                <xsl:if test="$setLevel != 0"><xsl:value-of select="concat('index', $setLevel - 1)" /></xsl:if>
                <xsl:if test="$setLevel = 0">null</xsl:if>
            </xsl:attribute>
            <xsl:attribute name=":this-set">thisSet</xsl:attribute>
            <xsl:call-template name="applyVisibilityForTree">
                <xsl:with-param name="fullName" select="$selectedParent" />
            </xsl:call-template>

            <xsl:element name="my-custom-template">
                <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />
                <ul class="{@name}">
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="parent" select="$selectedParent" />
                        <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
                        <xsl:with-param name="depthLevel" select="$depthLevel" />
                        <xsl:with-param name="setLevel" select="$setLevel" />
                    </xsl:apply-templates>
                </ul>
            </xsl:element>
        </DataclassItemComponent>
    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '']">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />
        <xsl:param name="setLevel" />

        <xsl:variable name="dataClass" select="@dataClass" />

        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages or //dataClass[ @name = $dataClass ]/@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:if test="($showDataclassInstancesInTree = true() or $meIsAPage = true()) and $depthLevel &lt;= $maxDepth">
            <li>
                <xsl:attribute name="class" select="$dataClass" />
                
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
                
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                />
                <xsl:call-template name="getTranslatedLabel" />
                <xsl:apply-templates select="//dataClass[@name = $dataClass]">
                    <xsl:with-param name="parent" select="$fullName" />
                    <xsl:with-param name="separateChildrenFromParent" select="$separateChildrenInPages" />
                    <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                    <xsl:with-param name="setLevel" select="$setLevel" />
                </xsl:apply-templates>
            </li>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>

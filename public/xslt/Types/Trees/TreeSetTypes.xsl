<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />
        <xsl:param name="setLevel" />

        <!-- TODO: unify with dataclass instance in DataClassTypes.xsl -->
        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="dataClass" select="@itemsDataClass" />

        <xsl:if test="($showSetsInTree = true() or $meIsAPage = true()) and $depthLevel &lt;= $maxDepth">

            <xsl:variable name="elementName">
                <xsl:call-template name="createElementName">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="setLevel" select="$setLevel" />
                </xsl:call-template>
            </xsl:variable>

            <SetComponent>
                <xsl:attribute name=":full-name" select="concat($apos, $fullName , $apos)" />
                <xsl:attribute name=":this-object">thisObject</xsl:attribute>
                <xsl:attribute name=":this-index">
                    <xsl:if test="$setLevel != 0"><xsl:value-of select="concat('index', $setLevel - 1)" /></xsl:if>
                    <xsl:if test="$setLevel = 0">null</xsl:if>
                </xsl:attribute>
                <xsl:attribute name=":model">model</xsl:attribute>
                <xsl:call-template name="applyVisibilityForTree">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />

                    <li>
                        <xsl:call-template name="getTranslatedLabel" />
                        <xsl:attribute name="class" select="$dataClass" />
                        
                        <xsl:call-template name="addID">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>

                        <xsl:attribute
                            name="v-on:click.stop"
                            select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                        />
                        <xsl:call-template name="applyVisibilityForTree">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>

                        <xsl:if
                            test="($showSetsInTree = true() or $separateChildrenInPages = true()) and ($depthLevel + 1) &lt;= $maxDepth"
                        >
                            <ul>
                                <li
                                    v-for="{ concat('(childElement, index', $setLevel, ') in thisSet') }"
                                    track-by="childElement.guid"
                                >
                                    <xsl:attribute
                                        name="v-on:click.stop"
                                        select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                                    />
                                    <xsl:attribute name="class" select="$dataClass" />
                                        
                                    <xsl:call-template name="addID">
                                        <xsl:with-param name="fullName" select="$fullName" />
                                    </xsl:call-template>

                                    <SetItemComponent>
                                        <xsl:attribute name=":full-name" select="concat($apos, $elementName, $apos)" />
                                        <xsl:attribute name=":this-set">thisSet</xsl:attribute>
                                        <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
                                        <xsl:attribute name=":model">model</xsl:attribute>

                                        <xsl:element name="my-custom-template">
                                            <xsl:attribute
                                                name="v-slot:default"
                                                select="'{ thisSet, thisObject, thisIndex }'"
                                            />
                                            <span>
                                                <xsl:variable
                                                    name="itemLabelVariable"
                                                    select="//dataClass[ @name = $dataClass ]/variable[ @itemLabel ]/@name"
                                                />
                                                <xsl:attribute name="v-html">
                                                    <xsl:if test="$itemLabelVariable">
                                                        <xsl:value-of
                                                            select="concat('(childElement.', $itemLabelVariable, '?.getLabel?.() ?? childElement.', $itemLabelVariable,
                                                            ') || translationService.t(', $apos, '-- new ', @name, ' --', $apos, ')')"
                                                        />
                                                    </xsl:if>
                                                    <xsl:if test="not($itemLabelVariable)">
                                                        <xsl:value-of
                                                            select="concat('translationService.t(', $apos, '-- new ', @name, ' --', $apos, ')')"
                                                        />
                                                    </xsl:if>
                                                </xsl:attribute>
                                            </span>
                                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                                <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                                <xsl:with-param name="parent" select="$elementName" />
                                                <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                                            </xsl:apply-templates>
                                        </xsl:element>
                                    </SetItemComponent>
                                </li>
                            </ul>
                        </xsl:if>
                    </li>
                </xsl:element>
            </SetComponent>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

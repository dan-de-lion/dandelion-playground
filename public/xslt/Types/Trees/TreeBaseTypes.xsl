<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template match="variable [ @dataType= 'group' ] ">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />
        <xsl:param name="setLevel">0</xsl:param>

        <!-- TODO: unify with dataclass instance in DataClassTypes.xsl -->
        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />    

        <xsl:if test="($showGroupsInTree = true() or $meIsAPage = true()) and $depthLevel &lt;= $maxDepth">
            <li>
                <xsl:call-template name="applyVisibilityForTree">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:attribute name="class" select="@name" />

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
                />

                <xsl:call-template name="getTranslatedLabel" />

                <ul>
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="parent" select="concat($fullName, '.')" />
                        <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
                        <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                        <xsl:with-param name="setLevel" select="$setLevel" />
                    </xsl:apply-templates>
                </ul>
            </li>
        </xsl:if>

    </xsl:template>

    <xsl:template match="variable">
        <!-- This is necessary to prevent other variable datatypes from doing anything -->
    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## All variables ######## -->

    <xsl:template match="variable [ @dataType != 'group' and @dataType != 'set' ]">
        "<xsl:value-of select="@name" />" : {
        <xsl:if test="@accessLevel">"accessLevel": "<xsl:value-of select="@accessLevel" />",
        </xsl:if>
        },
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'set' ]">
        <xsl:param name="parent" />

        <xsl:variable name="dataType" select="@itemsDataClass" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

            "<xsl:value-of select="@name" />" : {
                <xsl:if test="@accessLevel">"accessLevel": "<xsl:value-of select="@accessLevel" />",
                </xsl:if>

                <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
                    <xsl:with-param name="parent" select="$fullName" />
                </xsl:apply-templates>
            },
    </xsl:template>
</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="root">
        <xsl:apply-templates select="//variable[ @itemsDataClass ]" />
    </xsl:template>

    <xsl:template match="dataClass">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />

        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        {
        "itemsDataClassName": "<xsl:value-of select="@name" />",
        "itemsDataClassList": [
        <xsl:apply-templates select="./variable" mode="itemsDataClassChildren">
            <xsl:with-param name="setLevel" select="$setLevel" />
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>
        ]
        },
    </xsl:template>

    <xsl:template match="variable">
        <xsl:param name="parent" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="itemsDataClass" select="@itemsDataClass" />

        <xsl:apply-templates select="//dataClass[ @name = $itemsDataClass ]">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable" mode="itemsDataClassChildren">
         <xsl:variable name="position"><xsl:value-of select="position()" /></xsl:variable>
            <xsl:variable name="last"><xsl:value-of select="last()" /></xsl:variable>

            {
            "name" : "<xsl:value-of select="@name" />",
            "type" : "<xsl:value-of select="@dataType" />"
            <xsl:if test="((@dataType = 'singlechoice') or (@dataType = 'multiplechoice')) and @valuesSet">
                ,
                "valuesSet" : "<xsl:value-of select="@valuesSet" />"
            </xsl:if>
            <xsl:if test="((@dataType = 'singlechoice') or (@dataType = 'multiplechoice')) and not(@valuesSet)">
                ,
                "valuesSet" : "<xsl:value-of select="@name" />"
            </xsl:if>
            <xsl:if test="(@dataType = 'set')">
                ,
                "itemsDataClass" : "<xsl:value-of select="@itemsDataClass" />"
            </xsl:if>
            }

            <xsl:if test="$position &lt; $last">,</xsl:if>
    </xsl:template>
</xsl:stylesheet>

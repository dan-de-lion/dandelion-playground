<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="parent" />

        <!-- fullName is chosen differently if it's the first dataclass -->
        <xsl:variable name="fullName">
            <xsl:call-template name="selectFullName">
                <xsl:with-param name="fullName" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <!-- selectedParent is chosen differently if it's the first dataclass -->
        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <DataclassItemComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName , $apos)" />
            <xsl:attribute name=":model">model</xsl:attribute>
            <xsl:attribute name=":this-index">
                <xsl:if test="$setLevel != 0"><xsl:value-of select="concat('index', $setLevel - 1)" /></xsl:if>
                <xsl:if test="$setLevel = 0">null</xsl:if>
            </xsl:attribute>
            <xsl:attribute name=":this-set">thisSet</xsl:attribute>

            <xsl:element name="my-custom-template">
                <xsl:attribute name="v-slot:default" select="'{ thisSet, thisObject, thisIndex }'" />
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="setLevel" select="$setLevel" />
                        <xsl:with-param name="parent" select="$selectedParent" />
                    </xsl:apply-templates>

                    <xsl:apply-templates select="./warning">
                        <xsl:with-param name="parentName" select="$selectedParent" />
                    </xsl:apply-templates>
                </xsl:element>
        </DataclassItemComponent>
    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '']">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />
        <xsl:variable name="dataClass" select="@dataClass" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
            <xsl:with-param name="parent" select="$fullName" />
            <xsl:with-param name="setLevel" select="$setLevel" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./warning">
            <xsl:with-param name="parentName" select="$fullName" />
        </xsl:apply-templates>
    </xsl:template>

</xsl:stylesheet>

﻿<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../Utils.xsl" />
    <xsl:import href="../VariableAttributes.xsl" />
    <xsl:import href="../VariableStructure.xsl" />

    <xsl:import href="../Types/Filter/FilterBaseTypes.xsl" />
    <xsl:import href="../Types/Filter/FilterDataClassTypes.xsl" />

    <xsl:variable name="root">filterModel</xsl:variable>
    <xsl:param name="statisticianMode" />
    <xsl:param name="crfName" />
    <xsl:param name="crfVersion" />

    <xsl:variable name="apos">'</xsl:variable>

    <xsl:output method="html" indent="yes" />

    <xsl:variable name="cleanedRoot">
        <xsl:call-template name="createCleanedRoot" />
    </xsl:variable>
</xsl:stylesheet>

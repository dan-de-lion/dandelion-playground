<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../Utils.xsl" />
    <xsl:import href="../Types/DeveloperErrors/DeveloperErrorsBaseTypes.xsl" />
    <xsl:import href="../Types/DeveloperErrors/DeveloperErrorsDataClassTypes.xsl" />
    <xsl:import href="../Types/DeveloperErrors/DeveloperErrorsRequirements.xsl" />

    <xsl:param name="DEBUG" select="false()" />

    <xsl:output method="html" indent="yes" />

    <xsl:variable name="apos">'</xsl:variable>

    <xsl:param name="root" />
    <xsl:param name="statisticianMode" />
    <xsl:param name="crfName" />
    <xsl:param name="crfVersion" />

    <xsl:variable name="cleanedRoot">
        <xsl:call-template name="createCleanedRoot" />
    </xsl:variable>

</xsl:stylesheet>

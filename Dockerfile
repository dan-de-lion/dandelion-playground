FROM node:16-alpine
WORKDIR '/app'

## Install Vue CLI and, eventually, the package.json dependencies
RUN npm install -g @vue/cli

## Make user match with host user
ARG GROUP_ID
ARG USER_ID
RUN if ! getent group $GROUP_ID ; then addgroup --gid $GROUP_ID my_group ; fi
RUN if getent passwd $USER_ID ; then deluser "$(getent passwd $USER_ID | cut -d: -f1)" ; fi
RUN adduser --disabled-password --gecos '' -G "$(getent group $GROUP_ID | cut -d: -f1)" --uid $USER_ID my_user
USER my_user

EXPOSE 5174
EXPOSE 4174

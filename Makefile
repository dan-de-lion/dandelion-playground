
.PHONY: help init up down shell timelog

help:
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


internal_host = http://host.docker.internal:5174
##
## Available commands:
##---------------------------------------------------------------------------

init:  ## create dev environment
	docker build \
		--build-arg USER_ID=$(shell id -u) \
		--build-arg GROUP_ID=$(shell id -g) \
		-t dandelion-playground-dev -f Dockerfile .
	docker run -it --rm --name dandelion-playground -p 5174:5174 -v "$$(pwd):/app" -w /app dandelion-playground-dev npm install

up: ## run the dev environment
	docker run -it --rm --name dandelion-playground-dev -p 5174:5174 -p 4174:4174 -v "$$(pwd):/app" -w /app dandelion-playground-dev npm run dev

down: ## close the dev container
	docker stop dandelion-playground-dev

shell: ## open a shell inside the running dev container (needs make up)
	docker exec -it dandelion-playground-dev sh

timelog: ## get spent time recorded on Gitlab
	docker run --rm -it -v "$$(pwd)/.gtt:/root/.local/share/.gtt" kriskbx/gitlab-time-tracker report

link:
	sudo npm uninstall --save-dev @dandelion/core
	npm cache clean --force
	npm link @dandelion/core

unlink:
	sudo npm unlink @dandelion/core
	sudo npm cache clean --force
	sudo npm i @dandelion/core

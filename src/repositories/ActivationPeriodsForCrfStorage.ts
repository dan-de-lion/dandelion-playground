import { injectable } from 'inversify';
import { DandelionService } from "@dandelion/core";

@injectable()
export class ActivationPeriodsForCrfStorage extends DandelionService {
    constructor() {
        super();
    }

    async get() {
        const response = await fetch('/crfForCentreCode.json');
        if (response.ok) {
            return Promise.resolve(await response.json());
        }
        return Promise.resolve(undefined);
    }
}

import { injectable } from 'inversify';
import type { CaseListSnapshot, EventsStorageInterface, SourceEventForList } from '@dandelion/core';

@injectable()
export class StorageForEvents implements EventsStorageInterface {
  private localStorage = window.localStorage;

  constructor(private baseKey: string = 'events', private baseSnapshotKey = 'snapshotEvents') {}

  async loadAllEvents(centreCode: string, caseType: string): Promise<Array<SourceEventForList>> {
    const storages = Object.keys(localStorage);
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const eventTypes = storages.filter((storage) => storage.startsWith(eventKey));
    let events: Array<SourceEventForList> = [];

    for (const eventType of eventTypes) {
      const loadedEvents = JSON.parse(
        localStorage.getItem(eventType) ?? '[]'
      ) as Array<SourceEventForList>;
      events.push(
        ...loadedEvents.map((event) => ({ ...event, timestamp: new Date(event.timestamp) }))
      );
    }
    const sortedEvents = events.sort((a, b) => {
      return a.timestamp.getTime() - b.timestamp.getTime();
    });
    return sortedEvents;
  }

  async saveEvent(
    event: SourceEventForList,
    centreCode: string,
    caseType: string
  ): Promise<string> {
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const events = JSON.parse(localStorage.getItem(eventKey) ?? '[]') as Array<SourceEventForList>;
    events.push(event);
    this.localStorage.setItem(eventKey, JSON.stringify(events));
    return event.id;
  }

  async getSnapshotAndEvents(
    centreCode: string,
    caseType: string
  ): Promise<{ events: Array<SourceEventForList>; snapshot: CaseListSnapshot }> {
    const events = await this.loadAllEvents(centreCode, caseType);
    const snapshotKey = `${this.baseSnapshotKey}-${centreCode}-${caseType}`;
    const snapshots = JSON.parse(
      this.localStorage.getItem(snapshotKey) ?? '[]'
    ) as Array<CaseListSnapshot>;
    // if there is no snapshot, return all the events
    if (snapshots.length === 0) {
      return { events, snapshot: { cachedCaseList: [], date: new Date(''), numberOfEvents: 0 } };
    }
    // get the snapshot with the higher lastUpdate
    const lastSnapshot = snapshots.reduce((prev, current) =>
      prev.date > current.date ? prev : current
    );
    // impedance mismatch between Date and string
    lastSnapshot.date = new Date(lastSnapshot.date);
    // get the events after the lastUpdate of the snapshot
    const eventsAfterSnapshot = events.filter((event) => event.timestamp > lastSnapshot.date);
    return { events: eventsAfterSnapshot, snapshot: lastSnapshot };
  }

  async saveSnapshot(
    snapshot: CaseListSnapshot,
    centreCode: string,
    caseType: string
  ): Promise<boolean> {
    const snapshotKey = `${this.baseSnapshotKey}-${centreCode}-${caseType}`;
    const snapshots = JSON.parse(
      this.localStorage.getItem(snapshotKey) ?? '[]'
    ) as Array<CaseListSnapshot>;
    snapshot.date = new Date();
    snapshots.push(snapshot);
    this.localStorage.setItem(snapshotKey, JSON.stringify(snapshots));
    return true;
  }
}

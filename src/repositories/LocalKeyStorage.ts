import { injectable } from 'inversify';
import type { KeyStorageInterface } from '@dandelion/core';

@injectable()
export default class LocalKeyStorage implements KeyStorageInterface {
  private localStorage = window.localStorage;

  saveKey(uuid: string, key: string): Promise<boolean> {
    try {
      this.localStorage.setItem('key-' + uuid, key);
      return Promise.resolve(true);
    } catch (e) {
      return Promise.resolve(false);
    }
  }

  getKey(uuid: string): Promise<string> {
    try {
      const key = this.localStorage.getItem('key-' + uuid) ?? '';
      return Promise.resolve(key);
    } catch (e) {
      return Promise.resolve('');
    }
  }
}

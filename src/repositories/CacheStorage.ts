import { injectable } from 'inversify';
import type { VariablesCacheStorageInterface, CaseCache, CaseID } from '@dandelion/core';
import { Stringifier } from '../utils/SerDes';

const cacheStringifier = new Stringifier<Map<CaseID, CaseCache>>();
@injectable()
export class CacheStorage implements VariablesCacheStorageInterface {
  getCachedCases(): Map<string, CaseCache> {
    const localStorageCache = window.localStorage.getItem('cachedCases');
    return localStorageCache ? cacheStringifier.deserialize(localStorageCache) : new Map();
  }

  overwriteCache(cache: Map<string, CaseCache>): void {
    window.localStorage.setItem('cachedCases', cacheStringifier.serialize(cache));
  }
}

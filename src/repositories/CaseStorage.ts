import { injectable } from 'inversify';
import _ from 'lodash';
import { CaseStorageInterface, PartialStoredCase, HistoryPartialStoredCase } from "@dandelion/core";
import {Stringifier} from "../utils/SerDes";

@injectable()
export class CaseStorage implements CaseStorageInterface {
  lock(uuid: string): Promise<Date> {
    throw new Error('Method not implemented.');
  }
  unlock(uuid: string): Promise<void> {
    throw new Error('Method not implemented.');
  }
  private localStorage = window.localStorage;

  async delete(uuid: string): Promise<PartialStoredCase> {
    if (this.localStorage.getItem(`partials-${uuid}`) !== null) {
      const xCase = this.localStorage.getItem(`partials-${uuid}`);
      this.localStorage.setItem(`del-${uuid}`, xCase ?? '');
      this.localStorage.removeItem(`partials-${uuid}`);
      const result = new Stringifier<PartialStoredCase>().deserialize(xCase!);
      return result;
    } else {
      throw new Error(`failed to delete, no case with ${uuid}`);
    }
  }

  async permanentlyDelete(uuid: string): Promise<string> {
    const myItem = this.localStorage.getItem(`partials-${uuid}`);
    if (!myItem) {
      throw new Error(`failed to permanently delete, no case with ${uuid}`);
    }
    this.localStorage.removeItem(`partials-${uuid}`);
    return uuid;
  }

  async restore(uuid: string): Promise<PartialStoredCase> {
    if (this.localStorage.getItem(`del-${uuid}`) !== null) {
      const xCase = this.localStorage.getItem(`del-${uuid}`);
      this.localStorage.setItem(`partials-${uuid}`, xCase ?? '');
      this.localStorage.removeItem(`del-${uuid}`);
      const result = new Stringifier<PartialStoredCase>().deserialize(xCase!);
      return result;
    } else {
      throw new Error(`failed to restore, no case deleted with ${uuid}`);
    }
  }

  async getHistory(): Promise<Map<string, HistoryPartialStoredCase[]>> {
    const history = new Stringifier<Map<string, HistoryPartialStoredCase[]>>().deserialize(
      this.localStorage.getItem(`cases-history`)!
    );
    return history;
  }

  async getCaseHistory(caseID: string): Promise<HistoryPartialStoredCase[]> {
    const history = await this.getHistory();
    if (history && history.has(`partials-${caseID}`)) {
      return history.get(`partials-${caseID}`)!;
    }
    return [];
  }

  async getPartials(caseID: string): Promise<Array<PartialStoredCase>> {
    const partialStoredCases = new Stringifier<PartialStoredCase[]>().deserialize(
      this.localStorage.getItem(`partials-${caseID}`) ?? this.localStorage.getItem(`del-${caseID}`)!
    );

    const resultArray: Array<PartialStoredCase> = [];
    partialStoredCases.forEach((partial: PartialStoredCase) => {
      let partialMetaData = partial.metaData;

      partialMetaData = { ...partialMetaData, lastUpdate: new Date(partial.metaData.lastUpdate) };
      partialMetaData = {
        ...partialMetaData,
        creationDate: new Date(partial.metaData.creationDate),
      };
      // eslint-disable-next-line no-param-reassign
      partial = { ...partial, metaData: partialMetaData };

      resultArray.push(partial);
    });
    return resultArray;
  }

  async saveAllPartials(partialStoredCases: Array<PartialStoredCase>, isCaseUpdating?: boolean): Promise<void> {
    const partialStoredCasesKey = `partials-${partialStoredCases[0].caseID}`;
    if (isCaseUpdating) {
      const history: Map<string, HistoryPartialStoredCase[]> = (await this.getHistory()) ?? new Map();
      const oldCases = await this.getPartials(partialStoredCases[0].caseID);
      const historyCases = history.get(partialStoredCasesKey) || [];
      const historyCase: HistoryPartialStoredCase = {
        creationDate: new Date().toISOString(),
        case_: oldCases,
      };
      historyCases.push(historyCase);
      history.set(partialStoredCasesKey, historyCases);
      this.localStorage.setItem(`cases-history`, new Stringifier().serialize(_.cloneDeep(history)));
    }
    this.localStorage.setItem(partialStoredCasesKey, new Stringifier().serialize(_.cloneDeep(partialStoredCases)));
  }
}

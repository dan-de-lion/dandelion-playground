import PlaygroundCRF from "../entities/PlaygroundCRF";

export default class CRFStorage {
  private localStorage = window.localStorage;

  public setCRF(crf: PlaygroundCRF): void {
    const _crfs = this.localStorage.getItem('CRFs');
    const crfs: Array<PlaygroundCRF> = _crfs != null ? JSON.parse(_crfs) : [];

    const crfIndex = crfs.findIndex(_crf => (_crf.name === crf.name && _crf.version == crf.version));
    if(crfIndex > -1)
        crfs[crfIndex] = crf;
    else
      crfs.push(crf)

    this.localStorage.setItem('CRFs', JSON.stringify(crfs));
  }

  public removeCRF(crfName: string): void {
    const _crfs = this.localStorage.getItem('CRFs');

    const crfs: Array<PlaygroundCRF> = _crfs != null ? JSON.parse(_crfs) : [];

    const crfIndexToRemove = crfs.findIndex(crf => (crf.name === crfName));

    if(crfIndexToRemove >= 0) {
      crfs.splice(crfIndexToRemove, 1);
    }

    this.localStorage.setItem('CRFs', JSON.stringify(crfs));
  }

  public getCRF(crfName: string): PlaygroundCRF | null {
    const _crfs = this.localStorage.getItem('CRFs');
    const crfs: Array<PlaygroundCRF> = _crfs != null ? JSON.parse(_crfs) : [];

    return crfs.find(crf => crf.name === crfName) ?? null;
  }

  public getAllCRFs(): Array<PlaygroundCRF> {
    const _crfs = this.localStorage.getItem('CRFs');
    return _crfs != null ? JSON.parse(_crfs) : [];
  }
}

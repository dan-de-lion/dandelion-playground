import { createRouter, createWebHistory } from "vue-router";
import Playground from "../components/Playground.vue";
import CreateCRF from "../components/CreateCRF.vue"


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            redirect: "/create-crf"
        },
        {
            path: "/create-crf",
            name: "create-crf",
            component: CreateCRF,
        },
        {
            path: "/playground/:crfName",
            name: "playground",
            component: Playground,
        }
    ],
});

export default router;

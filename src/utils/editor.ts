import CodeMirror, { type Editor } from "codemirror";

export default class EditorHelper {
  static readonly schema: any = {
    "!top": [
      "root"
    ],
    "root": {
      "children": [
        "dataClass",
        "valuesSet"
      ]
    },
    "dataClass": {
      "attrs": {
        "name": null
      },
      "children": [
        "variable",
        "error",
        "warning",
        "requirement"
      ]
    },
    "variable": {
      "attrs": {
        "name": null,
        "dataType": ["text", "date"],
        "label": null,
        "tooltip": null,
        "required": null,
        "visible": null,
        "hideMode": null,
        "requiredForStatus": null,
        "computedFormula": null,
        "itemsDataType": null,
        "relationsElements": null,
        "elementsDataType": null,
        "maxDate": null,
        "minDate": null,
        "min": null,
        "max": null,
        "precision": null,
        "valuesSet": null,
        "valuesSetKey": null,
        "indexName": null,
        "indexClass": null,
        "interface": [
          "checkBox",
          "radio",
          "dropdown",
          "autocomplete",
          "textArea"
        ],
        "separateInPages": [
          "true"
        ],
        "saveAsSeparated": [
          "true"
        ],
        "itemKey": [
          "true"
        ],
        "itemLabel": [
          "true"
        ],
        "itemColor": [
          "true"
        ],
        "labelFromKey": [
          "true"
        ]
      },
      "children": [
        "variable",
        "error",
        "warning"
      ]
    },
    "error": {
      "attrs": {
        "if": null,
        "name": null,
        "requiredForStatus": null
      }
    },
    "warning": {
      "attrs": {
        "if": null,
        "name": null,
        "requiredForStatus": null
      }
    },
    "requirement": {
      "attrs": {
        "formula": null,
        "name": null,
        "requiredForStatus": null
      }
    },
    "valuesSet": {
      "attrs": {
        "name": null,
        "sourceType": [
          "table",
          "index"
        ]
      },
      "children": [
        "value",
        "class",
        "name",
        "table",
        "column"
      ]
    },
    "value": {
      "attrs": {
        "name": null,
        "label": null,
        "tooltip": null,
        "visible": null
      },
      "children": [
        "excludes"
      ]
    },
    "excludes": {},
    "class": {},
    "name": {},
    "table": {},
    "column": {}
  }


  public static completeAfter(cm: Editor, pred: () => any = () => false) {
    cm.getCursor();
    setTimeout(() => {
      if (!cm.state.completionActive)
        cm.showHint({ completeSingle: false });
    }, 100);
    return CodeMirror.Pass;
  }

  public static completeIfAfterLt(cm: Editor) {
    return EditorHelper.completeAfter(cm, () => {
      const cur = cm.getCursor();
      return cm.getRange(CodeMirror.Pos(cur.line, cur.ch - 1), cur) == "<";
    });
  }

  public static completeIfInTag(cm: Editor) {
    return EditorHelper.completeAfter(cm, () => {
      const tok = cm.getTokenAt(cm.getCursor());
      if (tok.type == "string" && (!/['"]/.test(tok.string.charAt(tok.string.length - 1)) || tok.string.length == 1)) return false;
      const inner = CodeMirror.innerMode(cm.getMode(), tok.state).state;
      return inner.tagName;
    });
  }

  // https://stackoverflow.com/questions/3913355/how-to-format-tidy-beautify-in-javascript
  public static formatHTML(html: string) {
    const tab = '\t';
    let result = '';
    let indent = '';

    html.split(/>\s*</).forEach((element) => {
      if (element.match(/^\/\w/)) {
        indent = indent.substring(tab.length);
      }

      result += `${indent}<${element}>\r\n`;

      if (element.match(/^<?\w[^>]*[^/]$/) && !element.startsWith('input')) {
        indent += tab;
      }
    });

    return result.substring(1, result.length - 3);
  }
}
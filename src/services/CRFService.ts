import { Version } from "@dandelion/core";
import { injectable } from "inversify-props";
import { CRFServiceInterface } from "@dandelion/core";
import type { CRFVersionValidity } from "@dandelion/core";
import PlaygroundCRF from "../entities/PlaygroundCRF";

@injectable()
// CRF Service used by the dandelion-core
export class CRFService implements CRFServiceInterface {
  getAccessLevelForCRF(crfName: string, _crfVersion: Version): any {
    return {};
  }
  //@ts-ignore
  getCRF(crfName: string, crfVersion: string): Promise<PlaygroundCRF> {
    const xml = JSON.parse(window.localStorage.getItem("currentXML") ?? "");
    const js = JSON.parse(window.localStorage.getItem("currentJS") ?? "");
    const css = JSON.parse(window.localStorage.getItem("currentCSS") ?? "");
    const translations = JSON.parse(
      window.localStorage.getItem("currentTranslations") ?? ""
    );
    return Promise.resolve(
      new PlaygroundCRF("sandbox", "0.0.0", xml, js, css, translations)
    );
  }

  getCRFActiveVersions(crfName: string): Promise<CRFVersionValidity[]> {
    return Promise.resolve([]);
  }
}

import { inject } from "inversify-props";
import CRFStorage from "../repositories/CRFStorage";
import PlaygroundCRF from "../entities/PlaygroundCRF";
import {NamedBindings} from "@dandelion/core";
export default class PlaygroundCRFService {
  @inject(NamedBindings.CRFStorage) private _storage: CRFStorage = new CRFStorage();

  getCRF(crfName: string): PlaygroundCRF {
    console.log(crfName)
    const crf = this._storage?.getCRF(crfName);
    if(!crf) {
      throw new Error('No CRF found');
    }
    return crf;
  }

  public setCRF(crf: PlaygroundCRF) {
    this._storage?.setCRF(crf);
  }

  public getAllCRFs(): Array<PlaygroundCRF> {
    if(this._storage === undefined) throw new Error('CRFs Storage not injected');
    return this._storage.getAllCRFs();
  }

  public getCRFModel(crfName: string,  modelIndex: number): any | undefined {
    const crf = this.getCRF(crfName);
    return crf.models[modelIndex];
  }

  public deleteCRF(crfName: string): void {
    return this._storage?.removeCRF(crfName);
  }


  public removeModelFromCRF(crfName: string, modelIndex: number) {
    const crf = this.getCRF(crfName);

    if(modelIndex < crf.models.length)
      crf.models.splice(modelIndex, 1);

    this.setCRF(crf);
  }

  public addModelToCRF(crfName: string, model: object, status: number = 3) {
    const crf = this.getCRF(crfName);

    crf.models.push(model);

    this.setCRF(crf);
  }
}

import { injectable } from 'inversify';
import {AccessRights} from "@dandelion/core";

@injectable()
export class UserService {
  public getCurrentRoles(): Array<string> {
    return ['simple_user'];
  }

  getAccessLevelsRights(): Map<string, AccessRights> {
    const accessRights = new Map<string, any>();

    accessRights.set('1', {
      read_roles: ['admin'],
      write_roles: ['admin'],
    });
    accessRights.set('2', {
      read_roles: ['simple_user', 'admin'],
      write_roles: ['simple_user', 'admin'],
    });
    accessRights.set('public', {
      read_roles: ['admin', 'simple_user'],
      write_roles: ['admin', 'simple_user'],
    });
    return accessRights;
  }

  canRead(_variable: string): boolean {
    return true;
  }

  canWrite(_variable: string): boolean {
    return true;
  }
}

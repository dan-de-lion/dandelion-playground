import "reflect-metadata";

import {createApp} from 'vue'
import App from './App.vue'
import router from "./router";
import {container} from "inversify-props"
import PlaygroundCRFService from "./services/PlaygroundCRFService";
import CRFStorage from "./repositories/CRFStorage";
import "./style.css"
import {CRFService} from "./services/CRFService";
// @ts-ignore
// import core configuration
import install, {CaseConfiguration, CoreConfiguration, Language, NamedBindings, setupContainers, ConsoleLogger, CRFVersionSelectionStrategy } from "@dandelion/core";
import {UserService} from "./services/UserService";
import {CaseStorage} from "./repositories/CaseStorage";
import {StorageForEvents} from "./repositories/StorageForEvents";
import {CacheStorage} from "./repositories/CacheStorage";
import LocalKeyStorage from "./repositories/LocalKeyStorage";
import {ActivationPeriodsForCrfStorage} from "./repositories/ActivationPeriodsForCrfStorage";

const app = createApp(App);

app.use(router);
app.use(install);

setupContainers( new CoreConfiguration({
    baseURL: import.meta.env.BASE_URL + "xslt/",
    baseURLForOverrides: undefined,
    encrypt: false,
    debug: false,
    statisticianMode: false,
    // @ts-ignore
    defaultLanguage: Language.en,
    listOfCaseTypes: [
        new CaseConfiguration({
            caseName: 'Admission',
            childrenCRFs: [
                {
                    crfName: 'Infection',
                },
                {
                    crfName: 'Covid',
                },
            ],
            caseLinkingConfiguration: [
                {
                    caseTypeName: 'Patient',
                    caseIDVariable: 'admission.patientId',
                },
            ],
            referenceDateVariableName: '',
        }),
    ],
}), [{ name: NamedBindings.CRFService, class: CRFService },
    { name: NamedBindings.UserService, class: UserService },
    { name: NamedBindings.Logger, class: ConsoleLogger },
    { name: NamedBindings.CaseStorage, class: CaseStorage },
    { name: NamedBindings.CRFVersionSelectionStrategy, class: CRFVersionSelectionStrategy },
    { name: NamedBindings.StorageForEvents, class: StorageForEvents },
    { name: NamedBindings.VariablesCacheStorage, class: CacheStorage },
    { name: NamedBindings.KeyStorage, class: LocalKeyStorage},
    { name: NamedBindings.ActivationPeriodForCentreCodeStorage, class: ActivationPeriodsForCrfStorage },
    ],

);

container.addSingleton(PlaygroundCRFService, NamedBindings.CRFService);
container.addSingleton(CRFStorage, NamedBindings.CRFStorage);

app.provide('container', container)
app.mount("#app");

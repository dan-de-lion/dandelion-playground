export default class PlaygroundCRF {
  public name: string;
  public version: string;
  public xml: string;
  public js: string;
  public css: string;
  public translations: string;
  public models: Array<any>;

  constructor(name: string, version: string, xml: string = '', js: string = '', css: string = '', translations: string = "", models: Array<any> = []) {
    this.name = name;
    this.version = version;
    this.xml = xml;
    this.js = js;
    this.css = css;
    this.translations = translations;
    this.models = models;
  }
}


